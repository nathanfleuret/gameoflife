﻿using System;
using System.Text;
using System.Threading;

namespace GameOfLife
{
    // Plays Conway's Game of Life on the console with a random initial state.
    class Program
    {
        // The delay in milliseconds between board updates.
        private const int DELAY = 400;

        // The cell colors.
        private const ConsoleColor DEAD_COLOR = ConsoleColor.White;
        private const ConsoleColor LIVE_COLOR = ConsoleColor.Black;
        private const ConsoleColor DYING_COLOR = ConsoleColor.DarkRed;
        private const ConsoleColor BIRTH_COLOR = ConsoleColor.Green;

        // The cell states.
        private const int DEAD = 0;
        private const int LIVE = 1;
        private const int DYING = 2;
        private const int BIRTH = 3;

        // The color of the cells that are off of the board.
        private const ConsoleColor EXTRA_COLOR = ConsoleColor.Gray;

        private const char EMPTY_BLOCK_CHAR = ' ';
        private const char FULL_BLOCK_CHAR = '\u2588';

        // Holds the current state of the board.
        private static int[,] board;

        // The dimensions of the board in cells.
        private static int width = 64;
        private static int height = 32;

        // True if cell rules can loop around edges.
        private static bool loopEdges = true;


        static void Main(string[] args)
        {
            initializeRandomBoard();

            initializeConsole();

            // Run the game until the Escape key is pressed.
            while (!Console.KeyAvailable || Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
                Program.drawBoard();
                Program.updateBoard();

                // Wait for a bit between updates.
                Thread.Sleep(DELAY);
            }
        }

        // Sets up the Console.
        private static void initializeConsole()
        {
            Console.BackgroundColor = EXTRA_COLOR;
            Console.Clear();

            Console.CursorVisible = false;

            // Each cell is two characters wide.
            // Using an extra row on the bottom to prevent scrolling when drawing the board.
            int width = Math.Max(Program.width, 8) * 2 + 1;
            int height = Math.Max(Program.height, 8) + 1;
            Console.SetWindowSize(width, height);
            Console.SetBufferSize(width, height);

            Console.BackgroundColor = DEAD_COLOR;
        }

        // Creates the initial board with a random state.
        private static void initializeRandomBoard()
        {
            var random = new Random();

            Program.board = new int[Program.width, Program.height];
            for (var y = 0; y < Program.height; y++)
            {
                for (var x = 0; x < Program.width; x++)
                {
                    //At first we only want dead of alive cells.
                    Program.board[x, y] = random.Next(2);
                }
            }
        }

        // Draws the board to the console.
        private static void drawBoard()
        {            
            //Write the string to the console.
            Console.SetCursorPosition(0, 0);
            for (var y = 0; y < Program.height; y++)
            {
                for (var x = 0; x < Program.width; x++)
                {
                    char c = Program.board[x, y] == 0 ? EMPTY_BLOCK_CHAR : FULL_BLOCK_CHAR;
                    switch (Program.board[x, y])
                    {
                        case 1:
                            Console.ForegroundColor = LIVE_COLOR;
                            break;
                        case 2:
                            Console.ForegroundColor = DYING_COLOR;
                            break;
                        case 3:
                            Console.ForegroundColor = BIRTH_COLOR;
                            break;
                        default:
                            break;
                    }
                    // Each cell is two characters wide.
                    Console.Write(c);
                    Console.Write(c);
                }
                Console.Write('\n');
            }
        }

        // Moves the board to the next state based on Conway's rules.
        private static void updateBoard()
        {
            // A temp variable to hold the next state while it's being calculated.
            int[,] newBoard = new int[Program.width, Program.height];

            for (var y = 0; y < Program.height; y++)
            {
                for (var x = 0; x < Program.width; x++)
                {
                    var n = countLiveNeighbors(x, y);
                    var c = Program.board[x, y];

                    // A live cell dies unless it has exactly 2 or 3 live neighbors.
                    // A dead cell remains dead unless it has exactly 3 live neighbors.
                    if (isAlive(c))
                    {
                        newBoard[x, y] = (n == 2 || n == 3) ? LIVE : DYING;
                    }
                    else
                    {
                        newBoard[x, y] = (n == 3) ? BIRTH : DEAD;
                    }
                }
            }

            // Set the board to its new state.
            Program.board = newBoard;
        }

        // Returns the number of live neighbors around the cell at position (x,y).
        private static int countLiveNeighbors(int x, int y)
        {
            // The number of live neighbors.
            int value = 0;

            // This nested loop enumerates the 9 cells in the specified cells neighborhood.
            for (var j = -1; j <= 1; j++)
            {
                // If loopEdges is set to false and y+j is off the board, continue.
                if (!Program.loopEdges && y + j < 0 || y + j >= Program.height)
                {
                    continue;
                }

                // Loop around the edges if y+j is off the board.
                int k = (y + j + Program.height) % Program.height;

                for (var i = -1; i <= 1; i++)
                {
                    // If loopEdges is set to false and x+i is off the board, continue.
                    if (!Program.loopEdges && x + i < 0 || x + i >= Program.width)
                    {
                        continue;
                    }

                    // Loop around the edges if x+i is off the board.
                    int h = (x + i + Program.width) % Program.width;

                    // Count the neighbor cell at (h,k) if it is alive.
                    value += isAlive(Program.board[h, k]) ? 1 : 0;
                }
            }

            // Subtract 1 if (x,y) is alive since we counted it as a neighbor.
            return value - (isAlive(Program.board[x, y]) ? 1 : 0);
        }

        private static bool isAlive(int value)
        {
            return value % 2 == 1;
        }
    }
}
